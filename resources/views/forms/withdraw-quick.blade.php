<form method="POST" name="quick-transaction-form">

    <input type="hidden" name="transaction_type" value="withdrawl" />

    <labal for="withdrawl-quick-reference">Reference</labal>
    <input type="text" name="reference" class="form-control" id="withdrawl-quick-reference" />

    <labal for="withdrawl-quick-amount">Amount</labal>

    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">{{ config('ledger.currency_symbol') }}</span>
        </div>
        <input type="number" min=0 name="amount" step="0.01" class="form-control" id="withdrawl-quick-amount" required/>
    </div>

    <labal for="deposit-quick-date">Date</labal>

    <div class="input-group">
        <input type="text" name="date" class="form-control" id="withdrawl-quick-date" value="{{ date('d/m/Y') }}" required/>
    </div>

    <input type="submit" value="submit" class="btn btn-warning btn-lg">

</form>
