@extends('layouts.app')

@section('content')

    <script type="text/javascript">
        $(function(){
            getCurrentBalance();
            getRecentTransactions();
            tryOverdrawnAlert();
            tryExcessAlert();
        })
    </script>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <div class="card">
                    <div class="card-header">
                        {{ Request::user()->name }}'s account
                    </div>
                    <div class="card-body">
                        <ul class="account-info-list">
                            <li>
                                <span class="account-label">Name</span> {{ Request::user()->name }}
                            </li>
                            <li>
                                <span class="account-label">Email</span> {{ Request::user()->email }}
                            </li>
                            <li>
                                <span class="account-label">Overdraft</span> &pound;{{ Request::user()->overdraft_in_pounds }}
                            </li>
                            <li>
                                <span class="account-label">Savings Alert</span> &pound;{{ Request::user()->excess_alert_in_pounds }}
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
