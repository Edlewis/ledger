@extends('layouts.app')

@section('content')

<script type="text/javascript">
    $(function(){
        getCurrentBalance();
        getRecentTransactions();
        tryOverdrawnAlert();
        tryExcessAlert();
    })
</script>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="alert alert-warning overdrawn" role="alert">
                You are overdrawn.
            </div>

            <div class="alert alert-warning excess" role="alert">
                You have in excess of &pound;{{ Request::user()->excess_alert_in_pounds }} in your account.
                Please consider arranging a call with one of our investment specialists.
            </div>

        </div>
    </div>
</div>


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card current-balance">
                <!--<div class="card-header">{{ Request::user()->name }}'s Dashboard</div>-->

                <div class="card-body">

                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <span class="current_balance">
                                    Your current balance is
                                    <span class="current-balance-value">
                                        ...
                                    </span>
                                </span>
                            </div>
                            <div class="col-md-6">
                                <span class="current_overdraft">
                                    Your overdraft is &pound;{{ Request::user()->overdraft_in_pounds }}
                                </span>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Submit a Deposit</div>
                <div class="card-body">
                    @include('forms.deposit-quick')
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Submit a Withdrawl</div>
                <div class="card-body">
                    @include('forms.withdraw-quick')
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">

            <div class="card recent-transactions">
                <div class="card-header">Recent Transactions</div>

                <div class="card-body">

                    <div class="recent-transactions-headers">
                        <span>type</span>
                        <span>value</span>
                        <span>reference</span>
                        <span>date</span>
                        <span>balance</span>
                    </div>

                    <ul class="recent-transactions-list">
                        <li class="template">
                            <span class="transaction_type"></span>
                            <span class="transaction_value"></span>
                            <span class="transaction_reference"></span>
                            <span class="transaction_date"></span>
                            <span class="transaction_balance"></span>
                        </li>
                    </ul>

                </div>
            </div>

        </div>
    </div>
</div>

@endsection
