var getCurrentBalanceAjax = null;

function getCurrentBalance(){

    if(getCurrentBalanceAjax !== null){
        getCurrentBalanceAjax.abort();
    }

    getCurrentBalanceAjax = $.ajax({
        url: "/user/balance",
        type: 'GET',
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
        data: {},
        dataType: 'json',
        success: function (res) {
            getCurrentBalanceAjax = null;
            updateCurrentBalance(res.balance);
        }
    });

}

var getRecentTransactionsAjax = null;

function getRecentTransactions(){

    if(getRecentTransactionsAjax !== null){
        getRecentTransactionsAjax.abort();
    }

    getRecentTransactionsAjax = $.ajax({
        url: "/user/recent-transactions",
        type: 'GET',
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
        data: {},
        dataType: 'json',
        success: function (res) {
            getRecentTransactionsAjax = null;
            updateRecentTransactions(res.transactions);
        }
    });

}

var tryOverdrawnAlertAjax = null;

function tryOverdrawnAlert(){

    if(tryOverdrawnAlertAjax !== null){
        tryOverdrawnAlertAjax.abort();
    }

    tryOverdrawnAlertAjax = $.ajax({
        url: "/user/should-show-overdrawn-alert",
        type: 'GET',
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
        data: {},
        dataType: 'json',
        success: function (res) {
            tryOverdrawnAlertAjax = null;
            showHideOverdraftAlert(res.show_alert);
        }
    });

}

function showHideOverdraftAlert(show){
    if(show){
        $('.alert-warning.overdrawn').show();
        return;
    }
    $('.alert-warning.overdrawn').hide();
}

var tryExcessAlertAjax = null;

function tryExcessAlert(){

    if(tryExcessAlertAjax !== null){
        tryExcessAlertAjax.abort();
    }

    tryExcessAlertAjax = $.ajax({
        url: "/user/should-show-excess-alert",
        type: 'GET',
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
        data: {},
        dataType: 'json',
        success: function (res) {
            tryExcessAlertAjax = null;
            showHideExcessAlert(res.show_alert);
        }
    });

}

function showHideExcessAlert(show){
    if(show){
        $('.alert-warning.excess').show();
        return;
    }
    $('.alert-warning.excess').hide();
}

function updateCurrentBalance(currentBalance){
    $('.current-balance .current-balance-value').html(currency_symbol+currentBalance);
}

function updateRecentTransactions(transactions){

    var recentTransactionsContainer = $('.recent-transactions .recent-transactions-list');
    var rowTemplate = $('li.template', recentTransactionsContainer);

    // remove any existing transactions
    $('.transaction', recentTransactionsContainer).remove();

    $.each(transactions, function(index, transaction){

        var row = rowTemplate.clone();
        row.removeClass('template').addClass('transaction');

        $('.transaction_type', row).html(transaction.type);
        $('.transaction_value', row).html(currency_symbol+transaction.value_in_pounds);
        $('.transaction_reference', row).html(transaction.reference);
        $('.transaction_date', row).html(transaction.transaction_date);
        $('.transaction_balance', row).html(currency_symbol+transaction.balance_value_in_pounds);

        recentTransactionsContainer.append(row)
    });
}

$(document).on('submit', 'form[name="quick-transaction-form"]', function(e){
    e.preventDefault();
    var formData = $(this).serializeArray();
    submitTransaction(formData, $(this));
})

var submitTransactionAjax = null;

function submitTransaction(formData, form){

    if(submitTransactionAjax !== null){
        submitTransactionAjax.abort();
    }

    submitTransactionAjax = $.ajax({
        url: "/transaction/store",
        type: 'POST',
        headers: {
            'X-CSRF-Token': $('meta[name=csrf-token]').attr('content')
        },
        data: formData,
        dataType: 'json',
        success: function (res) {
            submitTransactionAjax = null;

            // clear the form fields
            clearTransactionFormFields(form);

            // refresh the current balance and recent transactions list
            getCurrentBalance();
            getRecentTransactions();
            tryOverdrawnAlert();
            tryExcessAlert();
        }
    });

}

function clearTransactionFormFields(form){
    $('input[type="text"], input[type="number"]', form).val('');
}

$(function(){

    var options={
        format: 'dd/mm/yyyy',
        todayHighlight: true,
        autoclose: true,
    };

    $('input#deposit-quick-date').datepicker(options);
    $('input#withdrawl-quick-date').datepicker(options);
})

