<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Money;
use DB;

class Transaction extends Model
{

    protected $table = 'transactions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'value',
        'type',
        'reference',
        'from',
        'to',
        'transaction_date'
    ];
    protected $appends = [
        'value_in_pounds',
        'balance_value',
        'balance_value_in_pounds'
    ];

    /*
     * The user associated with this transaction
     */
    public function user(){
        return $this->belongsTo('App\User');
    }

    /*
     * The balance associaced with this transaction
     */
    public function balance(){
        return $this->hasOne('App\Balance');
    }

    public function getValueInPoundsAttribute(){
        return Money::fromPence($this->value)->inPoundsAndPence();
    }
    public function getBalanceValueAttribute(){
        return $this->balance->value;
    }
    public function getBalanceValueInPoundsAttribute(){
        return Money::fromPence($this->balance_value)->inPoundsAndPence();
    }
}
