<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Money;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getOverdraftAttribute(){
        // TODO: user settings
        return 25000;
    }
    public function getOverdraftInPoundsAttribute(){
        return Money::fromPence($this->overdraft)->inPoundsAndPence();
    }
    public function getExcessAlertAttribute(){
        // TODO: user settings
        return 400000;
    }
    public function getExcessAlertInPoundsAttribute(){
        return Money::fromPence($this->excess_alert)->inPoundsAndPence();
    }

    /*
     * All balances associated with this user
     */
    public function balances()
    {
        return $this->hasMany('App\Balance');
    }

    /*
     * Originally used to select the most recently-entered balance.
     * Now defunct, unless we remove the ability for users to enter transaction dates,
     * which we may do.
     */
    public function balance()
    {
        return $this->hasOne('App\Balance')->latest();
    }

    /*
     * This user's most "recent" balance value (actually their newest balance, which could be in the future...)
     * Recency as measured by the transaction_date, not created_at date
     * TODO: review this future-balance question.
     */
    public function getCurrentBalanceValueAttribute(){
        $most_recent_transaction = $this->transactions()
            ->orderBy('transaction_date', 'desc')
            ->orderBy('id', 'desc')
            ->first();

        if($most_recent_transaction !== null){
            $current_balance = $most_recent_transaction->balance->value;
        } else {
            $current_balance = 0;
        }
        return $current_balance;
    }

    /*
     * All transactions associated with this user.
     */
    public function transactions()
    {
        return $this->hasMany('App\Transaction');
    }
}
