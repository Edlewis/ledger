<?php namespace App\Libraries;

class Money {

	public static $pence;

	private static $_instance = null;

	private function __construct () { }

	/* Instantiate using a value in pounds, or pence */

	public static function fromPounds ($pounds)
	{
		if (self::$_instance === null) {
			self::$_instance = new self;
		}

		if(!is_numeric($pounds)){
		    $pounds = 0;
        }

        // remove unwanted characters
        $pounds = str_replace(',', '', $pounds);

		// multiply to pence (or other configured decimal)
		$pence = round($pounds * config('ledger.currency_multiplier'));

		// store the int-casted value
		self::$pence = (int) $pence;

		return self::$_instance;
	}
	public static function fromPence ($pence)
	{
		if (self::$_instance === null) {
			self::$_instance = new self;
		}

		self::$pence = (int) $pence;
		return self::$_instance;
	}

	/* Modify the value to have vat added, or to show only the vat figure */

	public function addVat() {
		self::$pence = self::$pence * ( 1 + config('ledger.vat_rate'));
		return $this;
	}
	public function onlyVat() {
		self::$pence = self::$pence * (config('ledger.vat_rate'));
		return $this;
	}

	/* Output in different formats */

	public function inPounds() {
		return number_format(self::$pence / config('ledger.currency_multiplier'), 0);
	}
	public function inPoundsNumeric() {
		return number_format(self::$pence / config('ledger.currency_multiplier'), 0, '.', '');
	}
	public function inPence() {
		return (string) self::$pence;
	}
	public function inPoundsAndPence() {
		return number_format(self::$pence / config('ledger.currency_multiplier'), config('ledger.currency_dp'));
	}
	public function inPoundsAndPenceNumeric() {
		return number_format(self::$pence / config('ledger.currency_multiplier'), config('ledger.currency_dp'), '.', '');
	}


}
