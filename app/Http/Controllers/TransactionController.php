<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use Money;
use DB;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the transactions index
     * TODO: make this do something
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('transactions');
    }

    public function store(Request $request){

        $validationRules = array(
            'transaction_type' => 'required|max:255|in:deposit,withdrawl',
            'reference' => 'sometimes|max:255',
            'amount'=>'required|numeric',
            'date'=>'required|date_format:d/m/Y'
        );
        $validator = Validator::make($request->all(), $validationRules);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        // carbonise the date
        $transaction_date = Carbon::createFromFormat('d/m/Y', $request->date);

        // get the balance value before this transaction
        $previousTransaction = $request->user()->transactions()
                ->where('transaction_date', '<=', $transaction_date)
                ->orderBy('transaction_date', 'desc')
                ->orderBy('id', 'desc')
                ->first();

        if($previousTransaction !== null){
            $previousBalance = $previousTransaction->balance->value;
        } else {
            $previousBalance = 0;
        }

        // if this is a withdrawl, the transaction value should be negative
        $transactionValue = Money::fromPounds($request->amount)->inPence();
        if($request->transaction_type == 'withdrawl'){
            $transactionValue = -$transactionValue;
        }

        // calculate the new balance
        $balanceValue = $previousBalance + $transactionValue;

        // Start database transaction. If we get any surprises, we'll roll it back.
        DB::transaction(function() use ($request, $transactionValue, $transaction_date, $balanceValue) {

            $transaction = $request->user()->transactions()->create([
                'type' => $request->transaction_type,
                'value' => $transactionValue,
                'reference' => strip_tags($request->reference),
                'transaction_date' => $transaction_date
            ]);

            $balance = $transaction->balance()->create([
                'user_id' => $request->user()->id,
                'value' => $balanceValue
            ]);

            // now this transaction is in place with the correct balance value, we need to walk through any
            // transactions dated after this one, and update the balances accordingly

            $postDateTransactions = $request->user()->transactions()
                ->where('transaction_date', '>', $transaction_date)
                ->orderBy('transaction_date')
                ->orderBy('id')
                ->get();

            foreach($postDateTransactions as $postDateTransaction){
                $balanceValue += $postDateTransaction->value;

                $postDateTransaction->balance()->update([
                    'value' => $balanceValue
                ]);
            }

            // if the new balance is below the overdraft limit, error?
            //if(current balance < -25000){
                #throw new \Exception('Unable to process transaction - not enough funds');
            //}

        });

        // if the new balance is above the notification threshold, notify the user
        // todo: user notification

        return response()->json([], 200);

    }
}
