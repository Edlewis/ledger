<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Money;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get the user's current balance.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBalance(Request $request)
    {

        return response()->json([
            'balance' => Money::fromPence($request->user()->current_balance_value)->inPoundsAndPence()
        ]);

    }
    /**
     * Get the user's recent transactions.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRecentTransactions(Request $request)
    {

        $recent_transactions = $request->user()->transactions()->orderBy('transaction_date', 'desc')->orderBy('id', 'desc')->limit(10)->get();
        return response()->json([
            'transactions' => $recent_transactions
        ]);

    }

    public function shouldShowOverdrawnAlert(Request $request){
        $show_alert = false;
        if($request->user()->current_balance_value < 0){
            $show_alert = true;
        }
        return response()->json([
            'show_alert' => $show_alert
        ]);
    }

    public function shouldShowExcessAlert(Request $request){
        $show_alert = false;
        if($request->user()->current_balance_value > $request->user()->excess_alert){
            $show_alert = true;
        }
        return response()->json([
            'show_alert' => $show_alert
        ]);
    }

}
