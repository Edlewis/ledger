<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{

    protected $table = 'balances';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'transaction_id',
        'value'
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function transactions(){
        return $this->hasMany('App\Transaction');
    }
}
