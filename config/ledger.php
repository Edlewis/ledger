<?php

return [

    'currency_symbol' => '£',
    'currency_dp' => 2,             // number of decimal places currencies have
    'currency_multiplier' => 100,   // used in conjunction with dps above

];
