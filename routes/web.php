<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/user/balance', 'UserController@getBalance')->name('getUserBalance');
Route::get('/user/recent-transactions', 'UserController@getRecentTransactions')->name('getRecentTransactions');
Route::get('/user/should-show-overdrawn-alert', 'UserController@shouldShowOverdrawnAlert')->name('shouldShowOverdrawnAlert');
Route::get('/user/should-show-excess-alert', 'UserController@shouldShowExcessAlert')->name('shouldShowExcessAlert');
Route::get('/account', 'AccountController@show')->name('account.show');
Route::post('/transaction/store', 'TransactionController@store')->name('storeTransaction');
